import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Register from "./components/auth/register.component";
import Login from "./components/auth/login.component";

import UserAdd from "./components/user/add-user.component";
import UserEdit from "./components/user/edit-user.component";
import UserList from "./components/user/list-user.component";

// import ServerAdd from "./components/server/add-server.component";
// import ServerDetail from "./components/server/detail-server.component";
import ServerList from "./components/server/list-server.component";

// import AppsAdd from "./components/apps/add-apps.component";
// import AppsDetail from "./components/apps/detail-apps.component";
import AppsList from "./components/apps/list-apps.component";

import { Nav, Navbar, NavDropdown } from 'react-bootstrap';

class App extends Component {
	constructor( props ) {
		super( props );
		this.is_login = localStorage.getItem('is_login');
	}
	render() {
		return (
			<div>
				<Nav className="navbar navbar-expand navbar-dark bg-dark" >
					<Link to={"/login"} className="navbar-brand">
						Memoora
					</Link>
					{ this.is_login ? 
						(
							<Navbar bg="transparent" variant="dark" expand="lg">
								<Navbar.Toggle aria-controls="basic-navbar-nav" />
								<Navbar.Collapse id="basic-navbar-nav">
									<Nav className="ml-auto">
										<Nav.Link href="/">Dashboard</Nav.Link>
										<NavDropdown title="User" 
											id="collasible-nav-dropdown" 
										>
											<NavDropdown.Item href={"/users"}>User</NavDropdown.Item>
											<NavDropdown.Divider />
											<NavDropdown.Item href={"/users/create"}>Add</NavDropdown.Item>
										</NavDropdown>
										<Nav.Link title="Server" href={"/servers"}>Server</Nav.Link>
										<Nav.Link title="Aplication" href={"/apps"}>Apps</Nav.Link>
									</Nav>
								</Navbar.Collapse>
							</Navbar>
						) : ("")
					}
				</Nav>
				<div 
					id="showContent" 
					className="form-group col-12"
				>
					<Switch>
						<Route exact path={["/", "/register"]} component={Register} />
						<Route exact path={["/login", "/login"]} component={Login} />

						<Route exact path="/users" component={UserList} />
						<Route exact path="/users/create" component={UserAdd} />
						<Route path="/users/edit/:id" component={UserEdit} />
						
						<Route exact path={["/servers", "/servers"]} component={ServerList} />
						<Route exact path={["/spps", "/apps"]} component={AppsList} />
					</Switch>
				</div>
			</div>
		);
	}
}

export default App;
