import React, { Component } from "react";
import UserDataService from "../../services/app.service";

export default class DetailUser extends Component {
	constructor(props) {
		super(props);
		this.onChangeUsername = this.onChangeUsername.bind(this);
		this.onChangeEmail = this.onChangeEmail.bind(this);
		this.onChangePassword = this.onChangePassword.bind(this);
		this.getUser = this.getUser.bind(this);
		this.updateIsActive = this.updateIsActive.bind(this);
		this.updateUser = this.updateUser.bind(this);
		this.deleteUser = this.deleteUser.bind(this);

		this.state = {
			currentUser: {
				user_id: null,
				user_username: "",
				user_email: "",
				user_password: "",
				user_is_active: false
			},
			message: ""
		};
	}

	componentDidMount() {
		this.getUser(this.props.match.params.id);
	}

	onChangeUsername(e) {
		const username = e.target.value;

		this.setState(function(prevState) {
			return {
				currentUser: {
					...prevState.currentUser,
					user_username: username
				}
			};
		});
	}

	onChangeEmail(e) {
		const email = e.target.value;
		
		this.setState(prevState => ({
			currentUser: {
				...prevState.currentUser,
				user_email: email
			}
		}));
	}

	onChangePassword(e) {
		const password = e.target.value;
		
		this.setState(prevState => ({
			currentUser: {
				...prevState.currentUser,
				user_password: password
			}
		}));
	}

	getUser(id) {
		UserDataService.get(id)
			.then(response => {
				this.setState({
					currentUser: response.data
				});
				console.log(response.data);
			})
			.catch(e => {
				console.log(e);
			});
	}

	updateIsActive( status ) {
		var data = {
			id: this.state.currentUser.id,
			username: this.state.currentUser.username,
			email: this.state.currentTutorial.email,
			password: this.state.currentTutorial.password,
			is_active: status
		};

		UserDataService.update(this.state.currentUser.id, data)
			.then(response => {
				this.setState(prevState => ({
					currentUser: {
						...prevState.currentUser,
						is_active: status
					}
				}));
				console.log(response.data);
			})
			.catch(e => {
				console.log(e);
			});
	}

	updateUser() {
		console.log(this.state.currentUser);
		UserDataService.update(
			this.state.currentUser.user_id,
			this.state.currentUser
		)
		.then(response => {
			console.log(response.data);
			this.setState({
				message: "The User was updated successfully!"
			});
		})
		.catch(e => {
			console.log(e);
		});
	}

	deleteUser() {
		UserDataService.delete(this.state.currentUser.user_id)
		.then(response => {
			console.log(response.data);
			this.props.history.push('/user')
		})
		.catch(e => {
			console.log(e);
		});
	}

	render() {
		const { currentUser } = this.state;

		return (
			<div className="form">
				{currentUser ? (
					<div className="edit-form">
						<h4>User</h4>
						<form>
							<div className="form-group">
								<label htmlFor="title">Username</label>
								<input
									type="text"
									className="form-control"
									id="username"
									value={currentUser.user_username || ''}
									onChange={this.onChangeUsername}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="email">Email</label>
								<input
									type="text"
									className="form-control"
									id="email"
									value={currentUser.user_email || ''}
									onChange={this.onChangeEmail}
								/>
							</div>

							<div className="form-group">
								<label htmlFor="password">Password</label>
								<input
									type="text"
									className="form-control"
									id="password"
									value={currentUser.user_password || ''}
									onChange={this.onChangePassword}
								/>
							</div>

							<div className="form-group">
								<label>
									<strong>Status:</strong>
								</label>
								{currentUser.user_is_active ? "Published" : "Pending"}
							</div>
						</form>

						{currentUser.user_is_active ? (
							<button
								className="badge badge-primary mr-2"
								onClick={() => this.updateIsActive(false)}
							>
								UnPublish
							</button>
						) : (
							<button
								className="badge badge-primary mr-2"
								onClick={() => this.updateIsActive(true)}
							>
								Publish
							</button>
						)}

						<button
							className="badge badge-danger mr-2"
							onClick={this.deleteUser}
						>
							Delete
						</button>

						<button
							type="submit"
							className="badge badge-success"
							onClick={this.updateUser}
						>
							Update
						</button>
						<p>{this.state.message}</p>
					</div>
				) : (
					<div>
						<br />
						<p>Please click on a User...</p>
					</div>
				)}
			</div>
		);
	}
}
