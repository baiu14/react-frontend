import React, { Component } from "react";
import DataService from "../../services/app.service";
import { Card, Table, Modal, Button } from 'react-bootstrap';
import { FaTable, FaFolderOpen, FaPlusSquare, FaWindowClose, FaRegEdit, FaRegListAlt, FaTrashAlt, FaSave } from "react-icons/fa";

export default class AppsList extends Component {
	constructor(props) {
		super( props );
		this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
		this.retrieveData = this.retrieveData.bind(this);
		this.refreshList = this.refreshList.bind(this);
		this.showModal = this.showModal.bind(this);
		this.removeData = this.removeData.bind(this);
		this.searchTitle = this.searchTitle.bind(this);
		this.changeComponent = this.changeComponent.bind(this);
		this.saveData = this.saveData.bind(this);
		this.updateData = this.updateData.bind(this);
		this.onChangeName = this.onChangeName.bind(this);
		this.onChangeIp = this.onChangeIp.bind(this);

		this.state = {
			data: [],
			row: null,
			rowIndex: -1,
			searchTitle: "",
			show: {
				table: true,
				modal: false,
				form: false,
				detail: false
			},
			server: [],
			message: false,
			submitted: false,
		};
	}

	changeComponent( par ) {
		var show = {...this.state.show};
		switch (par.name) {
			case "showTable":
				show.table = true;
				show.form = false;
				show.detail = false;
				
				break;
			case "showForm":
				show.table = false;
				show.form = true;
				show.detail = false;
				this.retrieveDataServer();
				break;
			case "showDetail":
				show.table = false;
				show.form = false;
				show.detail = true;
				break;
			default:
				break;
		}
		this.setState({ 
			show,
			row: par.row,
			rowIndex: par.index,
		});
	}
		
	componentDidMount() {
		this.retrieveData();
	}

	onChangeSearchTitle(e) {
		const searchTitle = e.target.value;
		this.setState({
			searchTitle: searchTitle
		});
	}
	
	retrieveData() {
		DataService.getAll()
			.then(response => {
				this.setState({
					data: response.data
				});
			})
			.catch(e => {
				console.log(e);
			});
	}

	retrieveDataServer() {
		let userId = localStorage.getItem('id')
		DataService.getUserServer( userId )
		.then(response => {
			this.setState({
				server: response.data
			});
		})
		.catch(e => {
			console.log(e);
		});
	}

	refreshList() {
		this.retrieveData();
		this.setState({
			row: null,
			rowIndex: -1
		});
	}

	showModal( row, index ) {
		this.setState({
			show: true,
			row: row,
			rowIndex: index
		});
	}

	removeData( id ) {
		let data = {
			"_id": id
		}
		DataService.delete( data )
		.then(response => {
			this.refreshList();
		})
		.catch(e => {
			console.log(e);
		});
	}

	searchTitle() {
		this.setState({
			row: null,
			rowIndex: -1
		});

		DataService.search(this.state.searchTitle)
		.then(response => {
			this.setState({
				data: response.data
			});
		})
		.catch(e => {
			console.log(e);
		});
	}

	saveData() {
		console.log( this.state.row );
		DataService.create( this.state.row )
			.then( response => {
				this.setState({
					row: response.data,
					submitted: true
				});
				this.refreshList();
				this.changeComponent({
					name: 'showTable', 
					row: null,
					index: null
				});
			})
			.catch(e => {
				console.log(e);
			});
	}

	updateData() {
		DataService.update(
			this.state.row._id,
			this.state.row
		)
		.then(response => {
			this.setState({
				message: "The Aplication was updated successfully!"
			});
			this.refreshList();
		})
		.catch(e => {
			console.log(e);
		});
	}

	onChangeName(e) {
		var row = {...this.state.row};
		row.name = e.target.value;
		this.setState({row})
		console.log( this.state );
	}

	onChangeIp(e) {
		var row = {...this.state.row};
		row.ip = e.target.value;
		this.setState({row})
		console.log( this.state.row );
	}

	render() {
		const { searchTitle, data, row, rowIndex, show, server } = this.state;
		return (
			<div>
				<div className="col-12"> 
					<Card>
						<Card.Header>
							<div style={{ fontSize: '18pt', fontWeight: 'bolder' }}>
								<FaFolderOpen  />&nbsp;Apps
							</div>
						</Card.Header>
						<Card.Body>
							{ show.table && (
								<div 
									id="table-area" 
									className="w-100"
								>
									<Card>
										<Card.Header>
											<div className="row">
												<div className="col-7" style={{ fontSize: '14pt', fontWeight: 'bold' }}>
													<FaTable />&nbsp;Table App
												</div>
												<div className="col-5">
													<div className="input-group">
														<input
															type="text"
															className="form-control"
															placeholder="Search App"
															value={searchTitle}
															onChange={this.onChangeSearchTitle}
														/>
														<div className="input-group-append">
															<button
																className="btn btn-outline-secondary"
																type="button"
																onClick={this.searchTitle}
															>
																Search
															</button>
														</div>
													</div>
												</div>
											</div>
										</Card.Header>
										<Card.Body>
											<div id="button-area" className="w-100 form-group">
												<button 
													className="btn btn-sm btn-success"
													onClick={() => this.changeComponent({
														name: 'showForm', 
														row: null,
														index: -1
													})}
												>
													<FaPlusSquare />&nbsp;Add
												</button>
											</div>
											<div id="show-table" className="w-100">
												<Table striped bordered hover>
													<thead>
														<tr>
															<th style={{ width: 10 }}>#</th>
															<th style={{ width: '25%' }}>App</th>
															<th style={{ width: '25%' }}>Username</th>
															<th style={{ width: '25%' }}>Ip</th>
															<th style={{ width: '15%' }}>Action</th>
														</tr>
													</thead>
													<tbody>
													{data && data.map((row, index) => (
														<tr key={index}>
															<td>{(index+1)}</td>
															<td>{row.name}</td>
															<td>{row.username}</td>
															<td>{row.ip}</td>
															<td>
																<Button 
																	className="btn btn-info btn-sm mr-1 ml-1"
																	onClick={() => this.changeComponent({
																		name: 'showDetail', 
																		row: row,
																		index: index
																	})}
																>
																	<FaRegListAlt />
																</Button>
																<Button 
																	className="btn btn-danger btn-sm mr-1 ml-1"
																	onClick={() => this.removeData( row._id )}
																>
																	<FaTrashAlt />
																</Button>
															</td>
														</tr>
													))}
													</tbody>
												</Table>
											</div>
										</Card.Body>
									</Card>
								</div>
							)}
							{ show.form && (
								<div
									id="form-area" 
									className="w-100"
								>
									<Card>
										<Card.Header>
											<div
												id="form-title"
												className="w-100 form-group"
												style={{ fontSize: '14pt', fontWeight: 'bold' }}
											>
												<FaRegEdit />&nbsp;
												Form { ( rowIndex >= 0 ? "Edit" : "Add" ) }
											</div>
										</Card.Header>
										<Card.Body>
											<div className="w-100 form-group">
												<div className="row">
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>App Name</b></label>
															<div className="col-7">
																<select 
																	name="name" 
																	className="form-control" 
																	value={(row ? row.name : "" )} 
																	onChange={this.onChangeName}
																>
																	<option value=""> -- Pilih Aplikasi -- </option>
																	<option value="nginx">Nginx</option>
																	<option value="apache">Apache</option>
																	<option value="mysql">MySql</option>
																	<option value="MongoDb">MongoDb</option>
																	<option value="nodejs">NodeJs</option>
																</select>
															</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Server</b></label>
															<div className="col-7">
																<select 
																	name="ip" 
																	className="form-control" 
																	value={(row ? row.ip : "")}
																	onChange={this.onChangeIp}
																>
																	<option value=""> -- Pilih Server -- </option>
																{ 
																	server && server.map((ser, index) => (
																	<option value={ser.ip}>{ser.ip}</option>
																	))
																}
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
										</Card.Body>
										<Card.Footer>
											<div className="w-100">
												<Button 
													className="btn btn-sm btn-danger mr-1 ml-1"
													onClick={() => this.changeComponent({
														name: 'showTable', 
														row: null,
														index: null
													})}
												><FaWindowClose />&nbsp;Close</Button>
												<Button 
													className="btn btn-sm btn-info mr-1 ml-1"
													 onClick={( rowIndex >= 0 ? this.updateData : this.saveData )}
												><FaSave />&nbsp;Save</Button>
											</div>
										</Card.Footer>
									</Card>
								</div>
							)}
							{ show.detail && (
								<div
									id="form-area" 
									className="w-100"
								>
									<Card>
										<Card.Header>
											<div
												id="form-title"
												className="w-100 form-group"
												style={{ fontSize: '14pt', fontWeight: 'bold' }}
											>
												<FaRegListAlt />&nbsp;
												Detail Aplication : { ( row ? row.name : "" ) }
											</div>
										</Card.Header>
										<Card.Body>
											<div className="w-100 form-group">
												<div className="row">
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Name</b></label>
															<div className="col-7">: {row.name}</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Ip</b></label>
															<div className="col-7">: {row.ip}</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Username</b></label>
															<div className="col-7">: { row.username }</div>
														</div>
													</div>
												</div>
											</div>
										</Card.Body>
										<Card.Footer>
											<div className="w-100">
												<Button 
													className="btn btn-sm btn-danger mr-1 ml-1"
													onClick={() => this.changeComponent({
														name: 'showTable', 
														row: null,
														index: null
													})}
												><FaWindowClose />&nbsp;Close</Button>
											</div>
										</Card.Footer>
									</Card>
								</div>
							)}
						</Card.Body>
					</Card>
					<Modal 
						size="xl"
						show={show.modal} 
						onHide={this.closeModal}
					>
						<Modal.Header closeButton>
							<Modal.Title>Detail Aplication</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<div className="col-12">
							</div>
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={this.closeModal}>
								Close
							</Button>
							<Button variant="primary" onClick={this.closeModal}>
								Save Changes
							</Button>
						</Modal.Footer>
					</Modal>
				</div>
			</div>
		);
	}
}