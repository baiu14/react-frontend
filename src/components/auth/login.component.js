import React, { Component } from "react";
import AuthDataService from "../../services/auth.service";
import { Link } from "react-router-dom";
import { Card, Button } from 'react-bootstrap';
import { FaLock } from "react-icons/fa";

export default class Register extends Component {
	constructor( props ) {
		super( props );
		this.login = this.login.bind(this);
		this.onChangeEmail = this.onChangeEmail.bind( this );
		this.onChangePassword = this.onChangePassword.bind( this );

		this.state = {
			row: null,
			submitted: false
		};
	}

	onChangeEmail( e ) {
		var row = {...this.state.row};
		row.email = e.target.value;
		this.setState({row})
	}

	onChangePassword( e ) {
		var row = {...this.state.row};
		row.password = e.target.value;
		this.setState({row})
	}

	login() {
		AuthDataService.login( this.state.row )
		.then(response => {
			this.setState({
				submitted: true
			});
			
			localStorage.setItem('is_login', true);
			localStorage.setItem('id', response.data.data.id);
			localStorage.setItem('token', response.data.data.token);
			localStorage.setItem('name', response.data.data.name);
			localStorage.setItem('username', response.data.data.username);
			localStorage.setItem('email', response.data.data.token)
			localStorage.setItem('country', response.data.data.country)
			this.props.history.push("/users");
		})
		.catch(e => {
			console.log(e);
		});
	}

	render() {
		return (
			<div className="submit-form">
				{this.state.submitted ? (
					<div>
						<h4>You submitted successfully!</h4>
						<button className="btn btn-success" onClick={this.newUser}>
							Add
						</button>
					</div>
				) : (
					<div>
						<Card>
							<Card.Header>
								<div style={{ fontSize: '18pt', fontWeight: 'bolder' }}>
								<FaLock  />&nbsp;Login
							</div>
							</Card.Header>
							<Card.Body>
								<div className="form-group">
									<label htmlFor="email">Email</label>
									<input
										type="text"
										className="form-control"
										id="email"
										required
										value={this.state.email}
										onChange={this.onChangeEmail}
										name="email"
									/>
								</div>

								<div className="form-group">
									<label htmlFor="password">Password</label>
									<input
										type="text"
										className="form-control"
										id="password"
										required
										value={this.state.password}
										onChange={this.onChangePassword}
										name="password"
									/>
								</div>

								<Button onClick={this.login} className="btn btn-success">
									Submit
								</Button>

								<div className="form-group">
									<p>Create Account ? <Link to={"/register"}>Register</Link></p>
								</div>
							</Card.Body>
						</Card>
					</div>
				)}
			</div>
		);
	}
}
