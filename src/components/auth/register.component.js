import React, { Component } from "react";
import AuthDataService from "../../services/auth.service";
import { Link } from "react-router-dom";
import { Card, Button } from 'react-bootstrap';
import { FaUserLock } from "react-icons/fa";

export default class Register extends Component {
	constructor( props ) {
		super( props );
		this.saveUser = this.saveUser.bind( this );
		this.newUser = this.newUser.bind( this );
		this.onChangeUsername = this.onChangeUsername.bind( this );
		this.onChangePassword = this.onChangePassword.bind( this );
		this.onChangeName = this.onChangeName.bind( this );
		this.onChangeEmail = this.onChangeEmail.bind( this );
		this.onChangeCountry = this.onChangeCountry.bind( this );

		this.state = {
			row: null,
			submitted: false,
			countries: []
		};

	}

	saveUser() {
		const { row } = this.state;
		AuthDataService.register( row )
			.then(response => {
				this.setState({
					id: response.data.id,
					name: response.data.username,
					email: response.data.email,
					password: response.data.password,
					country: response.data.country,

					submitted: true
				});
				console.log(response.data);
			})
			.catch(e => {
				console.log(e);
			});
	}

	newUser() {
		this.setState({
			row: null,
			submitted: false
		});
	}

	componentDidMount() {
		this.retrieveCountry();
	}

	retrieveCountry() {
		AuthDataService.get_country()
		.then(response => {
			this.setState({
				countries: response.data
			});
			console.log( response.data )
		})
		.catch(e => {
			console.log(e);
		});
	}

	onChangeUsername( e ) {
		var row = {...this.state.row};
		row.username = e.target.value;
		this.setState({row})
	}

	onChangeEmail( e ) {
		var row = {...this.state.row};
		row.email = e.target.value;
		this.setState({row})
	}

	onChangePassword( e ) {
		var row = {...this.state.row};
		row.password = e.target.value;
		this.setState({row})
	}

	onChangeName( e ) {
		var row = {...this.state.row};
		row.name = e.target.value;
		this.setState({row})
	}

	onChangeCountry( e ) {
		var row = {...this.state.row};
		row.country = e.target.value;
		this.setState({row})
	}

	render() {
		const { row, countries } = this.state;
		return (
			<div className="submit-form">
				{this.state.submitted ? (
					<div>
						<h4>You submitted successfully!</h4>
						<Link 
							to={"/login"} 
							className="btn btn-success" 
							onClick={this.newUser}
						>
							Login
						</Link>
					</div>
				) : (
					<div>
						<Card>
							<Card.Header>
								<div style={{ fontSize: '18pt', fontWeight: 'bolder' }}>
								<FaUserLock  />&nbsp;Register
							</div>
							</Card.Header>
							<Card.Body>
								<div className="form-group">
									<label htmlFor="name">Name</label>
									<input
										type="text"
										className="form-control"
										id="name"
										required
										value={(row ? row.name : "")}
										onChange={this.onChangeName}
										name="name"
									/>
								</div>

								<div className="form-group">
									<label htmlFor="username">Username</label>
									<input
										type="text"
										className="form-control"
										id="username"
										required
										value={(row ? row.username : "")}
										onChange={this.onChangeUsername}
										name="username"
									/>
								</div>

								<div className="form-group">
									<label htmlFor="email">Email</label>
									<input
										type="text"
										className="form-control"
										id="email"
										required
										value={(row ? row.email : "")}
										onChange={this.onChangeEmail}
										name="email"
									/>
								</div>

								<div className="form-group">
									<label htmlFor="password">Password</label>
									<input
										type="text"
										className="form-control"
										id="password"
										required
										value={(row ? row.password : "")}
										onChange={this.onChangePassword}
										name="password"
									/>
								</div>

								<div className="form-group">
									<label htmlFor="country">Country</label>
									<select 
											name="country" 
											className="form-control" 
											value={(row ? row.country : "")}
											onChange={this.onChangeCountry}
										>
											<option value=""> -- Select Country -- </option>
											{
												countries && countries.map((country, index) => (
													<option value={country.name.common}>{country.name.common}</option>
												))
											}
										</select>
								</div>

								<Button onClick={this.saveUser} className="btn btn-success">
									<FaUserLock  />&nbsp;Register
								</Button>

								<div className="form-group">
									<p>have a account ? <Link to={"/login"}>Login</Link></p>
								</div>
							</Card.Body>
						</Card>
					</div>
				)}
			</div>
		);
	}

}
