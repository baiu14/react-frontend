import React, { Component } from "react";
import DataServer from "../../services/server.service";
import { Card, Table, Modal, Button } from 'react-bootstrap';
import { FaTable, FaFolderOpen, FaPlusSquare, FaWindowClose, FaRegEdit, FaRegListAlt, FaTrashAlt, FaSave } from "react-icons/fa";

export default class ServerList extends Component {
	constructor(props) {
		super( props );
		this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
		this.retrieveData = this.retrieveData.bind(this);
		this.refreshList = this.refreshList.bind(this);
		this.showModal = this.showModal.bind(this);
		this.searchTitle = this.searchTitle.bind(this);
		this.changeComponent = this.changeComponent.bind(this);
		this.saveData = this.saveData.bind(this);
		this.updateData = this.updateData.bind(this);
		this.onChangeIp = this.onChangeIp.bind(this);
		this.onChangeUsername = this.onChangeUsername.bind(this);
		this.onChangeSshKey = this.onChangeSshKey.bind(this);

		this.state = {
			data: [],
			row: null,
			rowIndex: -1,
			searchTitle: "",
			show: {
				table: true,
				modal: false,
				form: false,
				detail: false
			},
			message: false,
			submitted: false,
		};
	}

	changeComponent( par ) {
		var show = {...this.state.show};
		switch (par.name) {
			case "showTable":
				show.table = true;
				show.form = false;
				show.detail = false;
				
				break;
			case "showForm":
				show.table = false;
				show.form = true;
				show.detail = false;
				break;
			case "showDetail":
				show.table = false;
				show.form = false;
				show.detail = true;
				break;
			default:
				break;
		}
		this.setState({ 
			show,
			row: par.row,
			rowIndex: par.index,
		});
	}
		
	componentDidMount() {
		this.retrieveData();
	}

	onChangeSearchTitle(e) {
		const searchTitle = e.target.value;
		this.setState({
			searchTitle: searchTitle
		});
	}

	retrieveData() {
		DataServer.getAll()
		.then(response => {
			this.setState({
				data: response.data
			});
			console.log(response.data);
		})
		.catch(e => {
			console.log(e);
		});
	}

	refreshList() {
		this.retrieveData();
		this.setState({
			row: null,
			rowIndex: -1
		});
	}

	showModal( row, index ) {
		this.setState({
			show: true,
			row: row,
			rowIndex: index
		});
	}

	searchTitle() {
		this.setState({
			row: null,
			rowIndex: -1
		});

		DataServer.search(this.state.searchTitle)
			.then(response => {
				this.setState({
					data: response.data
				});
			})
			.catch(e => {
				console.log(e);
			});
	}

	saveData() {
		let data = this.state.row;
		data.userId = localStorage.getItem("id");
		DataServer.create( data )
			.then( response => {
				this.setState({
					row: response.data,
					submitted: true
				});
				this.refreshList();
				this.changeComponent({
					name: 'showTable', 
					row: null,
					index: null
				});
			})
			.catch(e => {
				console.log(e);
			});
	}

	updateData() {
		DataServer.update(
			this.state.row._id,
			this.state.row
		)
		.then(response => {
			this.setState({
				message: "The Servers was updated successfully!"
			});
			this.refreshList();
			this.changeComponent({
				name: 'showTable', 
				row: null,
				index: null
			});
		})
		.catch(e => {
			console.log(e);
		});
	}

	removeData( id ) {
		let data = {
			"_id": id
		}
		DataServer.delete( data )
		.then(response => {
			console.log(response.data);
			this.refreshList();
		})
		.catch(e => {
			console.log(e);
		});
	}

	onChangeIp(e) {
		var row = {...this.state.row};
		row.ip = e.target.value;
		this.setState({row})
	}

	onChangeUsername(e) {
		var row = {...this.state.row};
		row.username = e.target.value;
		this.setState({row})
	}

	onChangeSshKey(e) {
		var row = {...this.state.row};
		row.sshkey = e.target.value;
		this.setState({row})
	}

	render() {
		const { searchTitle, data, row, rowIndex, show } = this.state;
		return (
			<div>
				<div className="col-12"> 
					<Card>
						<Card.Header>
							<div style={{ fontSize: '18pt', fontWeight: 'bolder' }}>
								<FaFolderOpen  />&nbsp;Servers
							</div>
						</Card.Header>
						<Card.Body>
							{ show.table && (
								<div 
									id="table-area" 
									className="w-100"
								>
									<Card>
										<Card.Header>
											<div className="row">
												<div className="col-7" style={{ fontSize: '14pt', fontWeight: 'bold' }}>
													<FaTable />&nbsp;Table Servers
												</div>
												<div className="col-5">
													<div className="input-group">
														<input
															type="text"
															className="form-control"
															placeholder="Search by ip"
															value={searchTitle}
															onChange={this.onChangeSearchTitle}
														/>
														<div className="input-group-append">
															<button
																className="btn btn-outline-secondary"
																type="button"
																onClick={this.searchTitle}
															>
																Search
															</button>
														</div>
													</div>
												</div>
											</div>
										</Card.Header>
										<Card.Body>
											<div id="button-area" className="w-100 form-group">
												<button 
													className="btn btn-sm btn-success"
													onClick={() => this.changeComponent({
														name: 'showForm', 
														row: null,
														index: -1
													})}
												>
													<FaPlusSquare />&nbsp;Add
												</button>
											</div>
											<div id="show-table" className="w-100">
												<Table striped bordered hover>
													<thead>
														<tr>
															<th style={{ width: 10 }}>#</th>
															<th style={{ width: '20%' }}>User ID</th>
															<th style={{ width: '20%' }}>Username</th>
															<th style={{ width: '15%' }}>IP</th>
															<th style={{ width: '25%' }}>SSHKEY</th>
															<th style={{ width: '15%' }}>Action</th>
														</tr>
													</thead>
													<tbody>
													{data && data.map((row, index) => (
														<tr key={index}>
															<td>{(index+1)}</td>
															<td>{row.userId}</td>
															<td>{row.username}</td>
															<td>{row.ip}</td>
															<td>{row.sshkey}</td>
															<td>
																<Button 
																	className="btn btn-info btn-sm mr-1 ml-1"
																	onClick={() => this.changeComponent({
																		name: 'showDetail', 
																		row: row,
																		index: index
																	})}
																>
																	<FaRegListAlt />
																</Button>
																<Button 
																	className="btn btn-warning btn-sm mr-1 ml-1"
																	onClick={() => this.changeComponent({
																		name: 'showForm', 
																		row: row,
																		index: index
																	})}
																>
																	<FaRegEdit />
																</Button>
																<Button 
																	className="btn btn-danger btn-sm mr-1 ml-1"
																	onClick={() => this.removeData(row._id)}
																>
																	<FaTrashAlt />
																</Button>
															</td>
														</tr>
													))}
													</tbody>
												</Table>
											</div>
										</Card.Body>
									</Card>
								</div>
							)}
							{ show.form && (
								<div
									id="form-area" 
									className="w-100"
								>
									<Card>
										<Card.Header>
											<div
												id="form-title"
												className="w-100 form-group"
												style={{ fontSize: '14pt', fontWeight: 'bold' }}
											>
												<FaRegEdit />&nbsp;
												Form { ( rowIndex >= 0 ? "Edit" : "Add" ) }
											</div>
										</Card.Header>
										<Card.Body>
											<div className="w-100 form-group">
												<div className="row">
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>User</b></label>
															<div className="col-7">
																<input type="hidden" name="id" defaultValue={(row ? row._id : "" )} />
																<input type="hidden" name="userId" defaultValue={(row ? row.id : localStorage.getItem('id') )} />
																<input 
																	name="user" 
																	className="form-control" 
																	defaultValue={localStorage.getItem('name')} 
																	onChange={this.onChangeTitle}
																	readOnly="readonly"
																/>
															</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Username</b></label>
															<div className="col-7">
																<input 
																	name="username" 
																	className="form-control" 
																	defaultValue={( row ? row.username : "" )} 
																	onChange={this.onChangeUsername}
																/>
															</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>IP</b></label>
															<div className="col-7">
																<input 
																	name="ip" 
																	className="form-control" 
																	defaultValue={( row ? row.ip : "" )} 
																	onChange={this.onChangeIp}
																/>
															</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>sshkey</b></label>
															<div className="col-7">
																<textarea 
																	name="sshkey" 
																	className="form-control" 
																	value={( row ? row.sshkey : "" )} 
																	onChange={this.onChangeSshKey}
																/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</Card.Body>
										<Card.Footer>
											<div className="w-100">
												<Button 
													className="btn btn-sm btn-danger mr-1 ml-1"
													onClick={() => this.changeComponent({
														name: 'showTable', 
														row: null,
														index: null
													})}
												><FaWindowClose />&nbsp;Close</Button>
												<Button 
													className="btn btn-sm btn-info mr-1 ml-1"
													 onClick={( rowIndex >= 0 ? this.updateData : this.saveData )}
												><FaSave />&nbsp;Save</Button>
											</div>
										</Card.Footer>
									</Card>
								</div>
							)}
							{ show.detail && (
								<div
									id="form-area" 
									className="w-100"
								>
									<Card>
										<Card.Header>
											<div
												id="form-title"
												className="w-100 form-group"
												style={{ fontSize: '14pt', fontWeight: 'bold' }}
											>
												<FaRegListAlt />&nbsp;
												Detail Server : { ( row ? row.username : "" ) }
											</div>
										</Card.Header>
										<Card.Body>
											<div className="w-100 form-group">
												<div className="row">
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Username</b></label>
															<div className="col-7">: {row.username}</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Ip</b></label>
															<div className="col-7">: {row.ip}</div>
														</div>
													</div>
													<div className="col-6">
														<div className="row form-group">
															<label className="col-5"><b>Ssh Key</b></label>
															<div className="col-7">: { row.sshkey }</div>
														</div>
													</div>
												</div>
											</div>
										</Card.Body>
										<Card.Footer>
											<div className="w-100">
												<Button 
													className="btn btn-sm btn-danger mr-1 ml-1"
													onClick={() => this.changeComponent({
														name: 'showTable', 
														row: null,
														index: null
													})}
												><FaWindowClose />&nbsp;Close</Button>
											</div>
										</Card.Footer>
									</Card>
								</div>
							)}
						</Card.Body>
					</Card>
					<Modal 
						size="xl"
						show={show.modal} 
						onHide={this.closeModal}
					>
						<Modal.Header closeButton>
							<Modal.Title>Detail Tutorial</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<div className="col-12">
							</div>
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={this.closeModal}>
								Close
							</Button>
							<Button variant="primary" onClick={this.closeModal}>
								Save Changes
							</Button>
						</Modal.Footer>
					</Modal>
				</div>
			</div>
		);
	}
}