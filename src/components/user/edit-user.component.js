import React, { Component } from "react";
import UserDataService from "../../services/user.service";
import { Link } from "react-router-dom";
import { Card, Button } from 'react-bootstrap';
import { FaEdit, FaSave, FaChevronCircleLeft } from "react-icons/fa";

export default class DetailUser extends Component {
	constructor(props) {
		super(props);
		this.onChangeUsername = this.onChangeUsername.bind(this);
		this.onChangeName = this.onChangeName.bind(this);
		this.onChangeEmail = this.onChangeEmail.bind(this);
		this.onChangeCountry = this.onChangeCountry.bind(this);
		this.getUser = this.getUser.bind(this);
		this.updateIsActive = this.updateIsActive.bind(this);
		this.updateUser = this.updateUser.bind(this);

		this.state = {
			currentUser: {
				user_id: null,
				user_username: "",
				user_email: "",
				user_password: "",
				user_is_active: false
			},
			message: ""
		};
	}

	componentDidMount() {
		this.getUser(this.props.match.params.id);
	}

	onChangeUsername(e) {
		const username = e.target.value;

		this.setState(function(prevState) {
			return {
				currentUser: {
					...prevState.currentUser,
					username: username
				}
			};
		});
	}

	onChangeName(e) {
		const name = e.target.value;

		this.setState(function(prevState) {
			return {
				currentUser: {
					...prevState.currentUser,
					name: name
				}
			};
		});
	}

	onChangeEmail(e) {
		const email = e.target.value;
		
		this.setState(prevState => ({
			currentUser: {
				...prevState.currentUser,
				email: email
			}
		}));
	}

	onChangeCountry(e) {
		const country = e.target.value;
		
		this.setState(prevState => ({
			currentUser: {
				...prevState.currentUser,
				country: country
			}
		}));
	}

	getUser(id) {
		UserDataService.get(id)
			.then(response => {
				this.setState({
					currentUser: response.data
				});
				console.log(response.data);
			})
			.catch(e => {
				console.log(e);
			});
	}

	updateIsActive( status ) {
		var data = {
			id: this.state.currentUser.id,
			username: this.state.currentUser.username,
			email: this.state.currentTutorial.email,
			password: this.state.currentTutorial.password,
			is_active: status
		};

		UserDataService.update(this.state.currentUser.id, data)
			.then(response => {
				this.setState(prevState => ({
					currentUser: {
						...prevState.currentUser,
						is_active: status
					}
				}));
				console.log(response.data);
			})
			.catch(e => {
				console.log(e);
			});
	}

	updateUser() {
		console.log(this.state.currentUser);
		UserDataService.update(
			this.state.currentUser._id,
			this.state.currentUser
		)
		.then(response => {
			console.log(response.data);
			this.setState({
				message: "The User was updated successfully!"
			});
		})
		.catch(e => {
			console.log(e);
		});
	}

	deleteUser() {
		UserDataService.delete(this.state.currentUser.user_id)
		.then(response => {
			console.log(response.data);
			this.props.history.push('/user')
		})
		.catch(e => {
			console.log(e);
		});
	}

	render() {
		const { currentUser } = this.state;

		return (
			<div className="form-group col-12">
				<Card>
					<Card.Header>
						<h4><FaEdit />&nbsp;Form Edit User</h4>
					</Card.Header>
					<Card.Body>
						<div className="form">
							{currentUser ? (
								<div className="edit-form">
									<form>
										<div className="form-group">
											<label htmlFor="title">Username</label>
											<input
												type="text"
												className="form-control"
												id="username"
												value={currentUser.username || ''}
												onChange={this.onChangeUsername}
											/>
										</div>
										<div className="form-group">
											<label htmlFor="title">Name</label>
											<input
												type="text"
												className="form-control"
												id="name"
												value={currentUser.name || ''}
												onChange={this.onChangeName}
											/>
										</div>
										<div className="form-group">
											<label htmlFor="email">Email</label>
											<input
												type="text"
												className="form-control"
												id="email"
												value={currentUser.email || ''}
												onChange={this.onChangeEmail}
											/>
										</div>

										<div className="form-group">
											<label htmlFor="country">Country</label>
											<input
												type="text"
												className="form-control"
												id="country"
												value={currentUser.country || ''}
												onChange={this.onChangeCountry}
											/>
										</div>
									</form>
									<Link 
										to="/users"
										className="btn btn-sm btn-danger m-1"
									>
										<FaChevronCircleLeft />&nbsp;Kembali
									</Link>
									<Button
										type="submit"
										className="btn btn-sm btn-success m-1"
										onClick={this.updateUser}
									><FaSave />&nbsp;Update
									</Button>
									<p>{this.state.message}</p>
								</div>
							) : (
								<div>
									<br />
									<p>Please click on a User...</p>
								</div>
							)}
						</div>
					</Card.Body>
				</Card>
			</div>
		);
	}
}
