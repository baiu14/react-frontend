import React, { Component } from "react";
import UserDataService from "../../services/user.service";
import { Link } from "react-router-dom";
import { Card, Table } from 'react-bootstrap';
import { FaTable, FaEdit, FaListAlt } from "react-icons/fa";


export default class UserList extends Component {
	constructor(props) {
		super(props);
		this.onChangeSearchText = this.onChangeSearchText.bind(this);
		this.retrieveUser = this.retrieveUser.bind(this);
		this.refreshList = this.refreshList.bind(this);
		this.setActiveUser = this.setActiveUser.bind(this);
		this.removeAllUser = this.removeAllUser.bind(this);
		this.searchText = this.searchText.bind(this);

		this.state = {
			User: [],
			currentUser: null,
			currentIndex: -1,
			searchTitle: ""
		};
	}

	componentDidMount() {
		this.retrieveUser();
	}

	onChangeSearchText(e) {
		const searchTitle = e.target.value;

		this.setState({
			searchTitle: searchTitle
		});
	}

	retrieveUser() {
		UserDataService.getAll()
			.then(response => {
				this.setState({
					User: response.data
				});
			})
			.catch(e => {
				console.log(e);
			});
	}

	refreshList() {
		this.retrieveUser();
		this.setState({
			currentUser: null,
			currentIndex: -1
		});
	}

	setActiveUser(User, index) {
		this.setState({
			currentUser: User,
			currentIndex: index
		});
	}

	removeAllUser() {
		UserDataService.deleteAll()
			.then(response => {
				console.log(response.data);
				this.refreshList();
			})
			.catch(e => {
				console.log(e);
			});
	}

	searchText() {
		this.setState({
			currentUser: null,
			currentIndex: -1
		});

		UserDataService.search(this.state.searchTitle)
			.then(response => {
				this.setState({
					User: response.data
				});
				console.log(response.data);
			})
			.catch(e => {
				console.log(e);
			});
	}

	render() {
		const { searchText, User, currentUser, currentIndex } = this.state;

		return (
			<div className="form-group col-12">
				<Card>
					<Card.Header>
						<h4><FaTable />&nbsp;User List</h4>
					</Card.Header>
					<Card.Body>
						<div className="form-group row"> 
							<div className="col-12"> 
								<div className="input-group mb-3">
									<input
										type="text"
										className="form-control"
										placeholder="Search by Name"
										value={searchText}
										onChange={this.onChangeSearchText}
									/>
									<div className="input-group-append">
										<button
											className="btn btn-outline-secondary"
											type="button"
											onClick={this.searchText}
										>
											Search
										</button>
									</div>
								</div>
							</div>
						</div>
						<div className="form-group row">
							<div className="col-8">
								<Table striped bordered hover>
									<thead>
										<tr>
											<th style={{ width: 10 }}>#</th>
											<th style={{ width: '25%' }}>Name</th>
											<th style={{ width: '25%' }}>Email</th>
											<th style={{ width: '25%' }}>Country</th>
											<th style={{ width: '15%' }}>Action</th>
										</tr>
									</thead>
									<tbody>
										{
											User && User.map((row, index) => (
										<tr 
											className={(
												index === currentIndex ? "active" : ""
											)}
											key={index}
										>
											<td>{(index+1)}</td>
											<td>{row.name}</td>
											<td>{row.email}</td>
											<td>{row.country}</td>
											<td>
												<button
													className="btn btn-sm btn-info m-1"
													onClick={() => this.setActiveUser(row, index)}
												><FaListAlt /></button>
											</td>
										</tr>
											))
										}
									</tbody>
								</Table>
							</div>
							<div className="col-4">
								{
									currentUser ? (
								<Card>
									<Card.Header>
										<h5>Detail</h5>
									</Card.Header>
									<Card.Body>
										<div className="row">
											<label className="col-5">
												<strong>Name:</strong>
											</label>
											<div className="col-7">
												{" "}
												{currentUser.name}
											</div>
										</div>
										<div className="row">
											<label className="col-5">
												<strong>Email:</strong>
											</label>
											<div className="col-7">
												{" "}
												{currentUser.email}
											</div>
										</div>
										<div className="row">
											<label className="col-5">
												<strong>Country:</strong>
											</label>
											<div className="col-7">
												{" "}
												{currentUser.country}
											</div>
										</div>
										<Link
											to={"/Users/edit/" + currentUser._id}
											className="btn btn-sm btn-warning"
										>
											<FaEdit />&nbsp;Edit
										</Link>
									</Card.Body>
								</Card>
									) : (
								<Card>
									<Card.Body>
										<br />
										<p>Please click on a User...</p>
									</Card.Body>
								</Card>
									)
								}
							</div>
						</div>
					</Card.Body>
				</Card>
			</div>
		);
	}
}
