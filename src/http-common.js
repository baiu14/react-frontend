import axios from "axios";

/*export default axios.create({
	baseURL: "http://localhost:5000/api",
	headers: {
		"Content-type": "application/json"
	}
});*/

export let http = axios.create({
	baseURL: "http://localhost:5000/api",
	headers: {
		"Content-type": "application/json"
	}
});

export let http_external = axios.create({
	baseURL: "https://restcountries.com/v3.1",
	headers: {
		"Content-type": "application/json",
	}
});
