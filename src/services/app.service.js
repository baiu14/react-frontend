import { http } from "../http-common";

class AppDataService {
	getAll() {
		return http.get( "/apps" );
	}

	get( id ) {
		return http.get( `/apps/${id}` );
	}

	create( data ) {
		return http.post( "/apps/create", data );
	}

	update( id, data ) {
		return http.put( `/apps/update/${id}`, data );
	}

	delete( data ) {
		return http.post( `/apps/remove`, data );
	}

	search( name ) {
		return http.get( `/apps/search?name=${name}` );
	}

	getUserServer( userId ) {
		return http.get( `/servers/byuser?userId=${userId}` );
	}
}

export default new AppDataService();