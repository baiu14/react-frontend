import { http, http_external } from "../http-common";

class AuthDataService {
	login( data ) {
		return http.post( "/login", data );
	}

	register( data ){
		return http.post( "/register", data );
	}

	get_country() {
		return http_external.get( "/all" );
	}
}

export default new AuthDataService();