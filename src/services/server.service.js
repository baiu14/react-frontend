import { http, http_external } from "../http-common";

class ServerDataService {
	getAll() {
		return http.get( "/servers" );
	}

	get( id ) {
		return http.get( `/servers/${id}` );
	}

	create( data ) {
		return http.post( "/servers/create", data );
	}

	update( id, data ) {
		return http.post( `/servers/update/${id}`, data );
	}

	delete( data ) {
		return http.post( "/servers/remove", data );
	}

	search( ip ) {
		return http.get( `/servers/search?ip=${ip}` );
	}

	get_country() {
		return http_external.get( "/all" );
	}
}

export default new ServerDataService();