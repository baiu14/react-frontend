import { http, http_external } from "../http-common";

class UserDataService {
	getAll() {
		return http.get( "/users" );
	}

	get( id ) {
		return http.get( `/users/findid/${id}` );
	}

	create( data ) {
		return http.post( "/users/create", data );
	}

	update( id, data ) {
		return http.post( `/users/update/${id}`, data );
	}

	delete( id ) {
		return http.delete( `/users/remove/${id}` );
	}

	search( name ) {
		return http.get( `/users/search?name=${name}` );
	}
	
	get_country() {
		return http_external.get( "/all" );
	}
}

export default new UserDataService();